package pe.com.enel.market.lib.generadorPDFeFactura;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pe.com.enel.market.lib.generadorPDFeFactura.beans.FacturaBean;
import pe.com.enel.market.lib.generadorPDFeFactura.beans.HojaBean;

/**
 * Hello world!
 *
 */
public class App 
{
	/* Argumentos:
	 * 1. FacturaBean
	 * 2. Parametro reportes Bean
	 * 3. Ruta Imagen
	 * 4. Ruta Plantillas
	 * 5. Ruta PDF
	 * */
	public static void main( String[] args )
    {
    	System.out.println( "init" );
    	//List <FacturaBean> dataBean = new ArrayList<FacturaBean>();
    	//dataBean.add(getDummy()); //parametro 1
    	//Map<Long,String> jasperFile = new HashMap<Long,String>(); //parametro 2
    	//jasperFile.put(new Long(1), "RES_Principal_20170923.jasper"); 
    	//jasperFile.put(new Long(2), "RES_Imagen_20170923.jasper");
    	//jasperFile.put(new Long(3), "RES_FISE_20170923 .jasper");   
    	//String rutaImg = "C:\\test\\";//parametro 3;
    	//String rutaPlantillas = "C:\\test\\"; // parametro 4
    	//String rutaPDF = "c:\\test\\"; //parametro 5
    	//GeneradorPDF generador = new GeneradorPDF();
    	//generador.generarPDF(dataBean, jasperFile, rutaImg, rutaPlantillas, rutaPDF);
	}


/*
	private static FacturaBean getDummy() {
		FacturaBean bean = new FacturaBean();
		bean.setIdCuenta(1161028);
		bean.setIdCabecera(new Long(3691));
		bean.setNroCuenta("1161028");
		bean.setNombreCorto("PAREDES CHUI ARMANDO            ");
		bean.setDireccionCorto("MZ.182 LT. 2,AH.SAN MARTIN DE PORRES -39  ");
		bean.setSistemaElectrico("LIMA      ");
		bean.setNroRuc("            ");
		bean.setNroRecibo("C-71707026");
		bean.setNroPagina("            ");
		bean.setAlimentador("I-03 ");
		bean.setTarifa("BT5B");
		bean.setPotContratada("2.20 kW     ");
		bean.setMedidorFase("MONOF�SICO - Electromec�nico");
		bean.setNroMedidor("07267238");
		bean.setCantHilos("3");
		bean.setTipoCnx("SUBTERRANEA");
		bean.setPlgTarifario("LIMA                     ");
		bean.setMesAnoFactu("ENERO 2017");
		bean.setFecLecAct("(13/01/2017)");
		bean.setLecAct("32831");
		bean.setFecLecAnt("(12/12/2016)");
		bean.setLecAnt("32428");
		bean.setFactor("1");
		bean.setDetCons("                          (13/01/17)           32831(12/12/16)           32428                         1                       403                    0.4872                                                    ");
		bean.setMesesCons("EnFeMrAbMyJnJlAgStOcNvDcEn");
		bean.setEscalaY("     0   118   236   354   472   590");
		bean.setConceptosTxt("Reposic. y Mant. de ConexCargo Fijo               Cargo por Energia        Interes Compensatorio    Alumbrado Publico                                 SUBTOTAL Mes Actual      I.G.V.                                            TOTAL Mes Actual         Aporte Ley N� 28749      Seguridad Energ�tica CASERedondeo Mes Anterior    Redondeo Mes Actual                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ");
		bean.setBarraConsumos("   371   343   338   405   345   302   378   358   436   469   429   354   403");
		bean.setConceptosImp("           1.31           2.50         196.34           0.32          14.70                        215.17          38.73                        253.90           3.26           5.00           0.45          -0.11                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ");
		bean.setFechaEmi("14/ENE/2017");
		bean.setRutaFact("80-120-7800-73");
		bean.setTension("220 V - BT");
		bean.setTipoCnxOsi("C1.1");
		bean.setMsgCliente("El total a pagar incluye: Recargo FOSE (Ley 27510) S/ 5.23,                               Categoria: Residencial, Nro de lote(s): 1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ");
		bean.setCodigoBarra("011610284000026250300120170378110000000001");
		bean.setEncCobrTxt("                                                                                                                             ");
		bean.setEncCobrImp("                                                                           ");
		bean.setEncTot("              ");
		bean.setTotConsumo(" S/    262.50 ");
		bean.setListInterrup("                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ");
		bean.setFseBeneficiario("Beneficiario: ");
		bean.setFseDni("DNI: ");
		bean.setFseDep("");
		bean.setFseValor("Valor de Descuento FISE: ");
		bean.setFseFecvenc("Fecha de Vencimiento: ");
		bean.setFseTicket("");
		bean.setFseTipo("F");
		bean.setTotalPagar("S/*****262.50");
		bean.setFlagCarita("1");
		bean.setFechaVcto("30/ENE/2017");
		bean.setFechaCorte("           ");
		bean.setPenulFactImp("269.50");
		bean.setPenulFactMes("Nov-16");
		bean.setUltFactMes("Dic-16");
		bean.setUltFactImp("232.50");
		bean.setNroSecuencia("00019");
		bean.setDistrito("LOS OLIVOS                    ");
		bean.setSaldoFavorTxt("                         ");
		bean.setSaldoFavorImp("              ");
		bean.setCarCtaTxt("               ");
		bean.setNomPropietario("                              ");
		bean.setTitularAmpliado("PAREDES CHUI ARMANDO                                                                                                                                                                                    ");
		bean.setDirTitAmpliado("MZ.182 LT. 2,AH.SAN MARTIN DE PORRES -39                                                                                                                                                                ");
		bean.setFechaProceso("01/01/2017");
		List<HojaBean> hojas = new ArrayList<HojaBean>();
		HojaBean hojaBean = new HojaBean();
		hojaBean.setIdDetalle(new Long(6));
		hojaBean.setIdParam(new Long(1));
		hojaBean.setNomImagen("IMG_RES_Carita_20170923_1.gif");
		hojaBean.setOrden(1);
		hojas.add(hojaBean);
		hojaBean = new HojaBean();
		hojaBean.setIdDetalle(new Long(7));
		hojaBean.setIdParam(new Long(2));
		hojaBean.setNomImagen("IMG_RES_Retira_20170923.jpg");
		hojaBean.setOrden(2);
		hojas.add(hojaBean);
		hojaBean = new HojaBean();
		hojaBean.setIdDetalle(new Long(7));
		hojaBean.setIdParam(new Long(3));
		hojaBean.setNomImagen("IMG_FISE_2_20170923.jpg");
		hojaBean.setOrden(3);
		hojas.add(hojaBean);
		bean.setHojaFactura(hojas);
		return bean;
	}*/
}
