package pe.com.enel.market.lib.generadorPDFeFactura.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class FacturaBean implements Serializable {
	private Long idCabecera;
	private Integer idCuenta;
	private String nombreArchivo;
	private String nroCuenta;
	private String nombreCorto;
	private String direccionCorto;
	private String sistemaElectrico;
	private String nroRuc;
	private String nroRecibo;
	private String nroPagina;
	private String alimentador;
	private String tarifa;
	private String potContratada;
	private String medidorFase;
	private String nroMedidor;
	private String cantHilos;
	private String tipoCnx;
	private String plgTarifario;
	private String mesAnoFactu;
	private String fecLecAct;
	private String lecAct;
	private String fecLecAnt;
	private String lecAnt;
	private String factor;
	private String detCons;
	private String mesesCons;
	private String escalaY;
	private String conceptosTxt;
	private String barraConsumos;
	private String conceptosImp;
	private String fechaEmi;
	private String rutaFact;
	private String tension;
	private String tipoCnxOsi;
	private String msgCliente;
	private String codigoBarra;
	private String encCobrTxt;
	private String encCobrImp;
	private String encTot;
	private String totConsumo;
	private String listInterrup;
	private String fseBeneficiario;
	private String fseDni;
	private String fseDep;
	private String fseValor;
	private String fseFecvenc;
	private String fseTicket;
	private String fseTipo;
	private String totalPagar;
	private String flagCarita;
	private String fechaVcto;
	private String fechaCorte;
	private String penulFactMes;
	private String penulFactImp;
	private String ultFactMes;
	private String ultFactImp;
	private String nroSecuencia;
	private String distrito;
	private String saldoFavorTxt;
	private String saldoFavorImp;
	private String carCtaTxt;
	private String nomPropietario;
	private String titularAmpliado;
	private String dirTitAmpliado;
	private Date fechaProceso;
	private String sfNroCuenta;
	private String sfNombre;
	private String sfTotDocumento;
	private Date sfFecVen;
	private String sfNroDocIdentidad;
	private String correo;
	private List<HojaBean> hojaFactura;
	private List<ConsumoBean> consumoFactura;
	
	public Long getIdCabecera() {
		return idCabecera;
	}
	public void setIdCabecera(Long idCabecera) {
		this.idCabecera = idCabecera;
	}
	public Integer getIdCuenta() {
		return idCuenta;
	}
	public void setIdCuenta(Integer idCuenta) {
		this.idCuenta = idCuenta;
	}
	public String getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(String nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public String getNombreCorto() {
		return nombreCorto;
	}
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}
	public String getDireccionCorto() {
		return direccionCorto;
	}
	public void setDireccionCorto(String direccionCorto) {
		this.direccionCorto = direccionCorto;
	}
	public String getSistemaElectrico() {
		return sistemaElectrico;
	}
	public void setSistemaElectrico(String sistemaElectrico) {
		this.sistemaElectrico = sistemaElectrico;
	}
	public String getNroRuc() {
		return nroRuc;
	}
	public void setNroRuc(String nroRuc) {
		this.nroRuc = nroRuc;
	}
	public String getNroRecibo() {
		return nroRecibo;
	}
	public void setNroRecibo(String nroRecibo) {
		this.nroRecibo = nroRecibo;
	}
	public String getNroPagina() {
		return nroPagina;
	}
	public void setNroPagina(String nroPagina) {
		this.nroPagina = nroPagina;
	}
	public String getAlimentador() {
		return alimentador;
	}
	public void setAlimentador(String alimentador) {
		this.alimentador = alimentador;
	}
	public String getTarifa() {
		return tarifa;
	}
	public void setTarifa(String tarifa) {
		this.tarifa = tarifa;
	}
	public String getPotContratada() {
		return potContratada;
	}
	public void setPotContratada(String potContratada) {
		this.potContratada = potContratada;
	}
	public String getMedidorFase() {
		return medidorFase;
	}
	public void setMedidorFase(String medidorFase) {
		this.medidorFase = medidorFase;
	}
	public String getNroMedidor() {
		return nroMedidor;
	}
	public void setNroMedidor(String nroMedidor) {
		this.nroMedidor = nroMedidor;
	}
	public String getCantHilos() {
		return cantHilos;
	}
	public void setCantHilos(String cantHilos) {
		this.cantHilos = cantHilos;
	}
	public String getTipoCnx() {
		return tipoCnx;
	}
	public void setTipoCnx(String tipoCnx) {
		this.tipoCnx = tipoCnx;
	}
	public String getPlgTarifario() {
		return plgTarifario;
	}
	public void setPlgTarifario(String plgTarifario) {
		this.plgTarifario = plgTarifario;
	}
	public String getMesAnoFactu() {
		return mesAnoFactu;
	}
	public void setMesAnoFactu(String mesAnoFactu) {
		this.mesAnoFactu = mesAnoFactu;
	}
	public String getFecLecAct() {
		return fecLecAct;
	}
	public void setFecLecAct(String fecLecAct) {
		this.fecLecAct = fecLecAct;
	}
	public String getLecAct() {
		return lecAct;
	}
	public void setLecAct(String lecAct) {
		this.lecAct = lecAct;
	}
	public String getFecLecAnt() {
		return fecLecAnt;
	}
	public void setFecLecAnt(String fecLecAnt) {
		this.fecLecAnt = fecLecAnt;
	}
	public String getLecAnt() {
		return lecAnt;
	}
	public void setLecAnt(String lecAnt) {
		this.lecAnt = lecAnt;
	}
	public String getFactor() {
		return factor;
	}
	public void setFactor(String factor) {
		this.factor = factor;
	}
	public String getDetCons() {
		return detCons;
	}
	public void setDetCons(String detCons) {
		this.detCons = detCons;
	}
	public String getMesesCons() {
		return mesesCons;
	}
	public void setMesesCons(String mesesCons) {
		this.mesesCons = mesesCons;
	}
	public String getEscalaY() {
		return escalaY;
	}
	public void setEscalaY(String escalaY) {
		this.escalaY = escalaY;
	}
	public String getConceptosTxt() {
		return conceptosTxt;
	}
	public void setConceptosTxt(String conceptosTxt) {
		this.conceptosTxt = conceptosTxt;
	}
	public String getBarraConsumos() {
		return barraConsumos;
	}
	public void setBarraConsumos(String barraConsumos) {
		this.barraConsumos = barraConsumos;
	}
	public String getConceptosImp() {
		return conceptosImp;
	}
	public void setConceptosImp(String conceptosImp) {
		this.conceptosImp = conceptosImp;
	}
	public String getFechaEmi() {
		return fechaEmi;
	}
	public void setFechaEmi(String fechaEmi) {
		this.fechaEmi = fechaEmi;
	}
	public String getRutaFact() {
		return rutaFact;
	}
	public void setRutaFact(String rutaFact) {
		this.rutaFact = rutaFact;
	}
	public String getTension() {
		return tension;
	}
	public void setTension(String tension) {
		this.tension = tension;
	}
	public String getTipoCnxOsi() {
		return tipoCnxOsi;
	}
	public void setTipoCnxOsi(String tipoCnxOsi) {
		this.tipoCnxOsi = tipoCnxOsi;
	}
	public String getMsgCliente() {
		return msgCliente;
	}
	public void setMsgCliente(String msgCliente) {
		this.msgCliente = msgCliente;
	}
	public String getCodigoBarra() {
		return codigoBarra;
	}
	public void setCodigoBarra(String codigoBarra) {
		this.codigoBarra = codigoBarra;
	}
	public String getEncCobrTxt() {
		return encCobrTxt;
	}
	public void setEncCobrTxt(String encCobrTxt) {
		this.encCobrTxt = encCobrTxt;
	}
	public String getEncCobrImp() {
		return encCobrImp;
	}
	public void setEncCobrImp(String encCobrImp) {
		this.encCobrImp = encCobrImp;
	}
	public String getEncTot() {
		return encTot;
	}
	public void setEncTot(String encTot) {
		this.encTot = encTot;
	}
	public String getTotConsumo() {
		return totConsumo;
	}
	public void setTotConsumo(String totConsumo) {
		this.totConsumo = totConsumo;
	}
	public String getListInterrup() {
		return listInterrup;
	}
	public void setListInterrup(String listInterrup) {
		this.listInterrup = listInterrup;
	}
	public String getFseBeneficiario() {
		return fseBeneficiario;
	}
	public void setFseBeneficiario(String fseBeneficiario) {
		this.fseBeneficiario = fseBeneficiario;
	}
	public String getFseDni() {
		return fseDni;
	}
	public void setFseDni(String fseDni) {
		this.fseDni = fseDni;
	}
	public String getFseDep() {
		return fseDep;
	}
	public void setFseDep(String fseDep) {
		this.fseDep = fseDep;
	}
	public String getFseValor() {
		return fseValor;
	}
	public void setFseValor(String fseValor) {
		this.fseValor = fseValor;
	}
	public String getFseFecvenc() {
		return fseFecvenc;
	}
	public void setFseFecvenc(String fseFecvenc) {
		this.fseFecvenc = fseFecvenc;
	}
	public String getFseTicket() {
		return fseTicket;
	}
	public void setFseTicket(String fseTicket) {
		this.fseTicket = fseTicket;
	}
	public String getFseTipo() {
		return fseTipo;
	}
	public void setFseTipo(String fseTipo) {
		this.fseTipo = fseTipo;
	}
	public String getTotalPagar() {
		return totalPagar;
	}
	public void setTotalPagar(String totalPagar) {
		this.totalPagar = totalPagar;
	}
	public String getFlagCarita() {
		return flagCarita;
	}
	public void setFlagCarita(String flagCarita) {
		this.flagCarita = flagCarita;
	}
	public String getFechaVcto() {
		return fechaVcto;
	}
	public void setFechaVcto(String fechaVcto) {
		this.fechaVcto = fechaVcto;
	}
	public String getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	public String getPenulFactMes() {
		return penulFactMes;
	}
	public void setPenulFactMes(String penulFactMes) {
		this.penulFactMes = penulFactMes;
	}
	public String getPenulFactImp() {
		return penulFactImp;
	}
	public void setPenulFactImp(String penulFactImp) {
		this.penulFactImp = penulFactImp;
	}
	public String getUltFactMes() {
		return ultFactMes;
	}
	public void setUltFactMes(String ultFactMes) {
		this.ultFactMes = ultFactMes;
	}
	public String getUltFactImp() {
		return ultFactImp;
	}
	public void setUltFactImp(String ultFactImp) {
		this.ultFactImp = ultFactImp;
	}
	public String getNroSecuencia() {
		return nroSecuencia;
	}
	public void setNroSecuencia(String nroSecuencia) {
		this.nroSecuencia = nroSecuencia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getSaldoFavorTxt() {
		return saldoFavorTxt;
	}
	public void setSaldoFavorTxt(String saldoFavorTxt) {
		this.saldoFavorTxt = saldoFavorTxt;
	}
	public String getSaldoFavorImp() {
		return saldoFavorImp;
	}
	public void setSaldoFavorImp(String saldoFavorImp) {
		this.saldoFavorImp = saldoFavorImp;
	}
	public String getCarCtaTxt() {
		return carCtaTxt;
	}
	public void setCarCtaTxt(String carCtaTxt) {
		this.carCtaTxt = carCtaTxt;
	}
	public String getNomPropietario() {
		return nomPropietario;
	}
	public void setNomPropietario(String nomPropietario) {
		this.nomPropietario = nomPropietario;
	}
	public String getTitularAmpliado() {
		return titularAmpliado;
	}
	public void setTitularAmpliado(String titularAmpliado) {
		this.titularAmpliado = titularAmpliado;
	}
	public String getDirTitAmpliado() {
		return dirTitAmpliado;
	}
	public void setDirTitAmpliado(String dirTitAmpliado) {
		this.dirTitAmpliado = dirTitAmpliado;
	}
	public List<HojaBean> getHojaFactura() {
		return hojaFactura;
	}
	public void setHojaFactura(List<HojaBean> hojaFactura) {
		this.hojaFactura = hojaFactura;
	}
	public List<ConsumoBean> getConsumoFactura() {
		return consumoFactura;
	}
	public void setConsumoFactura(List<ConsumoBean> consumoFactura) {
		this.consumoFactura = consumoFactura;
	}
	public Date getFechaProceso() {
		return fechaProceso;
	}
	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	public String getSfNroCuenta() {
		return sfNroCuenta;
	}
	public void setSfNroCuenta(String sfNroCuenta) {
		this.sfNroCuenta = sfNroCuenta;
	}
	public String getSfNombre() {
		return sfNombre;
	}
	public void setSfNombre(String sfNombre) {
		this.sfNombre = sfNombre;
	}
	public String getSfTotDocumento() {
		return sfTotDocumento;
	}
	public void setSfTotDocumento(String sfTotDocumento) {
		this.sfTotDocumento = sfTotDocumento;
	}
	public Date getSfFecVen() {
		return sfFecVen;
	}
	public void setSfFecVen(Date sfFecVen) {
		this.sfFecVen = sfFecVen;
	}
	public String getSfNroDocIdentidad() {
		return sfNroDocIdentidad;
	}
	public void setSfNroDocIdentidad(String sfNroDocIdentidad) {
		this.sfNroDocIdentidad = sfNroDocIdentidad;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	
}