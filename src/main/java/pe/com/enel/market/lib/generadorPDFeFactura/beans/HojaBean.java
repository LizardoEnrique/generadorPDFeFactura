package pe.com.enel.market.lib.generadorPDFeFactura.beans;

import java.io.Serializable;

public class HojaBean implements Serializable, Comparable<HojaBean>{

	private Long idDetalle;
	private FacturaBean factura;
	private Long idParam;
	private String nomImagen;
	private Integer rangoInicio;
	private Integer rangoFin;
	private Integer orden;
	
	public Long getIdDetalle() {
		return idDetalle;
	}
	public void setIdDetalle(Long idDetalle) {
		this.idDetalle = idDetalle;
	}
	public FacturaBean getFactura() {
		return factura;
	}
	public void setFactura(FacturaBean factura) {
		this.factura = factura;
	}
	public Long getIdParam() {
		return idParam;
	}
	public void setIdParam(Long idParam) {
		this.idParam = idParam;
	}
	public String getNomImagen() {
		return nomImagen;
	}
	public void setNomImagen(String nomImagen) {
		this.nomImagen = nomImagen;
	}
	public Integer getRangoInicio() {
		return rangoInicio;
	}
	public void setRangoInicio(Integer rangoInicio) {
		this.rangoInicio = rangoInicio;
	}
	public Integer getRangoFin() {
		return rangoFin;
	}
	public void setRangoFin(Integer rangoFin) {
		this.rangoFin = rangoFin;
	}
	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	public int compareTo(HojaBean o) {
		// TODO Auto-generated method stub
		return this.orden.compareTo(o.getOrden());
	}

	
}
