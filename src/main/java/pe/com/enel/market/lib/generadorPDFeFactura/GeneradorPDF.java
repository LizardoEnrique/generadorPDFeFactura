package pe.com.enel.market.lib.generadorPDFeFactura;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.google.gson.Gson;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import pe.com.enel.market.lib.generadorPDFeFactura.beans.ConsumoBean;
import pe.com.enel.market.lib.generadorPDFeFactura.beans.FacturaBean;
import pe.com.enel.market.lib.generadorPDFeFactura.beans.HojaBean;
import pe.com.enel.market.lib.generadorPDFeFactura.util.Constantes;

public class GeneradorPDF {

	public int generarPDF(String dataBean, Map<Long,String> jasperFile, String rutaImg, String rutaPlantillas, String rutaPDF){
		try {
			List <FacturaBean> list = new ArrayList<FacturaBean>();
			list = deserialize(dataBean, FacturaBean[].class);
			return generarPDF(list, jasperFile, rutaImg, rutaPlantillas, rutaPDF);
		} catch (Exception e) {
			return 0;
		} 
	}
	
	 public <T> List<T> deserialize(String s, Class<T[]> objClass) throws IOException, ClassNotFoundException {
		 Gson gson = new Gson();
		 T[] arr = gson.fromJson(s, objClass);
		 return Arrays.asList(arr);
	 }
	
	@SuppressWarnings("unchecked")
	public int generarPDF(List dataBean, Map<Long,String> jasperFile, String rutaImg, String rutaPlantillas, String rutaPDF){
	        int procesados = 0;
	  for (FacturaBean fact : (List<FacturaBean>) dataBean){
	        String carpetaPDF = getStringDate(fact.getFechaProceso(), "yyyy_MM");
	        rutaPDF+=carpetaPDF+File.separator;
	        File directory = new File(rutaPDF);
	       if (! directory.exists()){
	           directory.mkdir();
	       }
	         try {

	    		List<JasperPrint> jasperPrint = new ArrayList<JasperPrint>();
	    		formatoConsumos(fact);
	    		List <FacturaBean> datos = new ArrayList<FacturaBean>();
	    		datos.add(fact);
	    		
	    		List<HojaBean> hojas = fact.getHojaFactura();
	    		Collections.sort(hojas, new Comparator<HojaBean>() {

					public int compare(HojaBean o1, HojaBean o2) {
						// TODO Auto-generated method stub
						return o1.compareTo(o2);
					}
	    			
				});
	    		
	    		//GENERACION PDF POR HOJA
		    	for (HojaBean hoja : hojas) {
		    		int hojaRango = 1;
		    		if (hoja.getRangoInicio()!=null && hoja.getRangoFin()!=null){
		    			hojaRango = hoja.getRangoFin() - hoja.getRangoInicio() + 1;
		    		}
		    		for (int i=0; i<hojaRango;i++){
			    		Map<String, Object> parameters = new HashMap<String, Object>();
			    		parameters.put("PAR_RUTA_IMG", rutaImg);
			    		parameters.put("SUBREPORT_DIR", rutaPlantillas);
			    		parameters.put("PAR_NOMBRE_IMG", hoja.getNomImagen().replaceAll("\\$\\{Variable\\}", (i+1)+""));		   
			    		JRDataSource ds = new JRBeanCollectionDataSource(datos);
			    		jasperPrint.add(JasperFillManager.fillReport(rutaPlantillas+jasperFile.get(hoja.getIdParam()),parameters, ds));			    		
		    		}
		    	}
				   JRPdfExporter exporter = new JRPdfExporter();
				   exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrint)); //Set as export input my list with JasperPrint s
				   exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(rutaPDF+fact.getNombreArchivo()+Constantes.SUFIX_PE)); //or any other out streaam
				   SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
				   configuration.setCreatingBatchModeBookmarks(true); //add this so your bookmarks work, you may set other parameters
				   exporter.setConfiguration(configuration);
				   exporter.exportReport();
				   procesados++;
			} catch (Exception e) {
				e.printStackTrace();
				return 0;
			}
	    	System.out.println("Fin de generación de boleta: " + fact.getNroRecibo() +". Id: " + fact.getIdCabecera());
    	}
        return procesados;
    }

	public String getStringDate(Date date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	public void formatoConsumos(FacturaBean fact) throws ParseException {
        List<ConsumoBean> consumos = new ArrayList<ConsumoBean>();
        if (fact.getPenulFactImp()==null)fact.setPenulFactImp("");
        if (fact.getPenulFactMes()==null)fact.setPenulFactMes("");
        if (fact.getUltFactImp()==null)fact.setUltFactImp("");
        if (fact.getUltFactMes()==null)fact.setUltFactMes("");
        for (int i = 0; i < 13; i++){
               String consumo = "0";
               String mes = "";
               if (i*6 < fact.getBarraConsumos().length()){
                      consumo = fact.getBarraConsumos().substring(i*6, Math.min((i+1)*6,fact.getBarraConsumos().length()));
               }
               if (i*2 < fact.getMesesCons().length()){
                      mes = fact.getMesesCons().substring(i*2, Math.min((i+1)*2,fact.getMesesCons().length()));
               }
               if (consumo.trim().isEmpty()){
                      consumo = "0";
               }
               ConsumoBean cons = new ConsumoBean();
               cons.setCons1(new Long((i!=12)?consumo.trim():"0"));
               cons.setCons2(new Long((i==12)?consumo.trim():"0"));
               cons.setConsumo(new Long(consumo.trim()));
               cons.setMes(new Long(i+1));
               cons.setMesC(mes);
               if (i==12){
                      cons.setMesC(mes+" ");
               }
               
               consumos.add(cons);
        }
        fact.setConsumoFactura(consumos);
  }

	
	public String getMes(Date fechaProceso, int i) throws ParseException {
		Calendar cal = Calendar.getInstance(); 
		cal.setTime(fechaProceso);
		cal.add(Calendar.MONTH, i);
		return cal.getDisplayName(Calendar.MONTH, Calendar.SHORT_FORMAT, Locale.getDefault());
	}
	
	private String getDateAsString(Date date, String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}
}
