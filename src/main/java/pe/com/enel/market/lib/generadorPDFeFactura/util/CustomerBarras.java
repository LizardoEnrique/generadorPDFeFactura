package pe.com.enel.market.lib.generadorPDFeFactura.util;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.ui.RectangleInsets;

import net.sf.jasperreports.engine.JRAbstractChartCustomizer;
import net.sf.jasperreports.engine.JRChart;

public class CustomerBarras extends JRAbstractChartCustomizer   {

	public void customize(JFreeChart chart, JRChart jasperChart) {
		CategoryPlot categoryPlot = chart.getCategoryPlot();
		BarRenderer renderer = (BarRenderer) categoryPlot.getRenderer();
	 
		//Spaces between bars
		renderer.setItemMargin(1.2);
		
		//width between bars
		renderer.setMaximumBarWidth(0.04);
		
		//chart.getXYPlot().setAxisOffset(new RectangleInsets(5.0, 0.0, 5.0, 5.0));
		//categoryPlot.setLowerMargin(0.0);
		//renderer.setUpperMargin(0.0);
		
	}

}
