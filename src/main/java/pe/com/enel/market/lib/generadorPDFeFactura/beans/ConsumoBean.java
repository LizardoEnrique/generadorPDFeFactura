package pe.com.enel.market.lib.generadorPDFeFactura.beans;

import java.io.Serializable;

public class ConsumoBean implements Serializable{

	private Long idConsumo;
	private FacturaBean factura;
	private Long mes;
	private Long consumo;
	private Long cons1;
	private Long cons2;
	private String  mesC;
	public Long getIdConsumo() {
		return idConsumo;
	}
	public void setIdConsumo(Long idConsumo) {
		this.idConsumo = idConsumo;
	}
	public FacturaBean getFactura() {
		return factura;
	}
	public void setFactura(FacturaBean factura) {
		this.factura = factura;
	}
	public Long getMes() {
		return mes;
	}
	public void setMes(Long mes) {
		this.mes = mes;
	}
	public Long getConsumo() {
		return consumo;
	}
	public void setConsumo(Long consumo) {
		this.consumo = consumo;
	}
	public Long getCons1() {
		return cons1;
	}
	public void setCons1(Long cons1) {
		this.cons1 = cons1;
	}
	public Long getCons2() {
		return cons2;
	}
	public void setCons2(Long cons2) {
		this.cons2 = cons2;
	}
	public String getMesC() {
		return mesC;
	}
	public void setMesC(String mesC) {
		this.mesC = mesC;
	}
	
	
}
